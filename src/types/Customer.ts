export default interface Customer {
  id?: number;
  name: string;
  surname: string;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}
