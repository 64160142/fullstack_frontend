import { ref } from "vue";
import { defineStore } from "pinia";
import type Customer from "@/types/Customer";

import customerService from "@/services/customer";

import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
export const usecustomerStore = defineStore("Customer", () => {
  const loadingStore = useLoadingStore();
  const MessageStore = useMessageStore();
  const dialog = ref(true);
  const customer = ref<Customer[]>([]);
  const editedcustomer = ref<Customer>({ name: "", surname: "" });
  async function getCustomer() {
    loadingStore.isLoading = true;

    try {
      const res = await customerService.getCustomer();
      customer.value = res.data;
      console.log(res);
    } catch (e) {
      console.log(e);
      MessageStore.showError("ไม่สามารถดึงข้อมูล Customer ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveCustomers() {
    loadingStore.isLoading = true;

    try {
      if (editedcustomer.value.id) {
        const res = await customerService.updateCustomer(
          editedcustomer.value.id,
          editedcustomer.value
        );
      } else {
        const res = await customerService.saveCustomers(editedcustomer.value);
      }
      dialog.value = false;
      clearCustomer();
      await getCustomer();
    } catch (e) {
      console.log(e);
      MessageStore.showError("ไม่สามารถบันทึกข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }

  async function deleteCustomer(id: number) {
    loadingStore.isLoading = true;

    try {
      const res = await customerService.deleteCustomer(id);
      await getCustomer();
    } catch (e) {
      console.log(e);
      MessageStore.showError("ไม่สามารถลบข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }

  function editCustomer(customer: Customer) {
    editedcustomer.value = JSON.parse(JSON.stringify(customer));
    dialog.value = true;
  }

  function clearCustomer() {
    editedcustomer.value = { name: "", surname: "" };
  }
  loadingStore.isLoading = false;

  return {
    customer,
    getCustomer,
    dialog,
    editedcustomer,
    saveCustomers,
    editCustomer,
    deleteCustomer,
  };
});
