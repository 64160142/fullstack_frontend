import { ref } from "vue";
import { defineStore } from "pinia";
import type Product from "@/types/Product";
import productService from "@/services/product";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
export const useProductStore = defineStore("Product", () => {
  const loadingStore = useLoadingStore();
  const MessageStore = useMessageStore();
  const dialog = ref(false);
  const products = ref<Product[]>([]);
  const editedProduct = ref<Product>({ name: "", price: 0 });
  async function getProducts() {
    loadingStore.isLoading = true;

    try {
      const res = await productService.getProducts();
      products.value = res.data;
      console.log(res);
    } catch (e) {
      console.log(e);
      MessageStore.showError("ไม่สามารถดึงข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveProduct() {
    loadingStore.isLoading = true;

    try {
      if (editedProduct.value.id) {
        const res = await productService.updateproduct(
          editedProduct.value.id,
          editedProduct.value
        );
      } else {
        const res = await productService.saveProduct(editedProduct.value);
      }
      dialog.value = false;
      clearProduct();
      await getProducts();
    } catch (e) {
      console.log(e);
      MessageStore.showError("ไม่สามารถบันทึกข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }

  async function deleteproduct(id: number) {
    loadingStore.isLoading = true;

    try {
      const res = await productService.deleteproduct(id);
      await getProducts();
    } catch (e) {
      console.log(e);
      MessageStore.showError("ไม่สามารถลบข้อมูล Product ได้");
    }
    loadingStore.isLoading = false;
  }

  function editProduct(product: Product) {
    loadingStore.isLoading = true;

    editedProduct.value = JSON.parse(JSON.stringify(product));
    dialog.value = true;
  }

  function clearProduct() {
    editedProduct.value = { name: "", price: 0 };
  }
  loadingStore.isLoading = false;

  return {
    products,
    getProducts,
    dialog,
    editedProduct,
    saveProduct,
    editProduct,
    deleteproduct,
  };
});
